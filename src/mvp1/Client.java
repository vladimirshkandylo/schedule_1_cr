package mvp1;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.net.*;
import java.io.*;
import java.util.Scanner;

public class Client {

    public static void main (String[] args) throws IOException {
        //задание переменных сканера, сокета, ридера и райтера
        Scanner sc = new Scanner(System.in);
        Socket clientSocket = null;
        BufferedReader reader = null;
        BufferedWriter writer = null;
        //создание сокета 
        try {
            clientSocket = new Socket("127.0.0.1", 8000);
        }
        catch (IOException ex) {
            System.out.println("Не получается подключиться к порту");
        }
        //создание ридера из входящего потока сокета 
        try {
            assert clientSocket != null;
            reader = new BufferedReader(
                    new InputStreamReader(
                            clientSocket.getInputStream()));
        }
        catch (IOException e) {
            System.out.println("Не удалось получить поток ввода");
        }
        //создание райтера из выходящего потока сокета 
        try {
            writer = new BufferedWriter(
                    new OutputStreamWriter(
                            clientSocket.getOutputStream()));
        }
        catch (IOException e) {
            System.out.println("Не удалось получить поток вывода");
        }

        //интерфейс для пользователя и получение от него информации 
        System.out.println("Выберите данные, которые вы хотите получить: \n"
                        + "1. Всё расписание.\n"
                        + "2. Список преподавателей.\n"
                        + "3. Список групп.");
        String request = sc.nextLine();

        // отправка запроса на сервер
        writer.write(request + "\n");
        writer.flush();

        // получение ответа от сервера
        String response = "";
        while((response = reader.readLine()) != null) {
            System.out.println(response);
        }
        //закрытие райтера, ридера и сокета 
        writer.close();
        reader.close();

        clientSocket.close();

    }
}

