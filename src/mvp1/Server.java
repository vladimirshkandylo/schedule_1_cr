//метод main можно разбить на несколько более мелких методов, использовать подготовленные выражения 
package mvp1;
//импорты
import java.net.*;
import java.io.*;
import java.sql.*;
import java.io.BufferedReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Server {
    //задание статических переменных класса для 
    static String url = "jdbc:mysql://localhost:3306/schedule";
    static String username = "root";
    static String password = "";

    public static void main (String[] args) throws InterruptedException {
        //установка соединения с БД mysql и выброс ошибки в случае исключения 
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            System.out.println("Соединение с базой данных установлено.");
        } catch (SQLException e) {
            throw new IllegalStateException("Не получается подключиться к базе данных!", e);
        }
        //создание переменной длясчета клиентов, строки через StringBuilder и пустого серверного сокета
        ServerSocket serverSocket = null;

        StringBuilder sb = new StringBuilder();
        int clientCount = 0;

        /* создаём serverSocket, который будет подключен к порту (8000)
            и будет ожидать подключений */
        try {
            serverSocket = new ServerSocket(8000);
            System.out.println("Сервер запущен.");
        }
        catch (IOException ex) {
            System.out.println("Не получается запустить сервер с данным портом");
        }

        try {
            while (true) {
                /* получаем подключения от клиента, как только клиент подключился,
                у клиентского сокета (clientSocket) выполняется команда создание сокета
                (вызова коструктора)*/
                Socket clientSocket = serverSocket.accept();
                System.out.println("Клиент подключен " + (++clientCount));

                // поток для отправления сообщений через сокет
                BufferedWriter writer = new BufferedWriter(
                        new OutputStreamWriter(
                                clientSocket.getOutputStream()));

                // поток для получения сообщений через сокет
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(
                                clientSocket.getInputStream()));

                // чтение запроса от клиента
                String request = reader.readLine();
                Thread.sleep(3000);
                if (request.equals("/closeServer")){
                    writer.write("Сервер отключен.");
                    writer.flush();

                    // закрываем потоки ввода/вывода и сокет
                    writer.close();
                    reader.close();
                    clientSocket.close();

                    System.out.println("Сервер отключен.");
                }
                //проверка полученного реквеста от пользователя 
                if (request.equals("1")){
                    try (Connection conn = DriverManager.getConnection(url, username, password)){
                            
                            //соединение с БД, создание запроса для БД
                            Statement statement = conn.createStatement();
                            ResultSet resultSet = statement.executeQuery("SELECT s.idSchedule, f.facultyName, " +
                                    "f.facultyShortName, g.numberStudents, g.groupName, sp.specialityName, " +
                                    "sp.specialityShortName, a.audienceName, t.teacherSurname, t.teacherName, " +
                                    "t.teacherPatronymic, sbj.subjectName, dow.dayOfWeek, tol.typeOfLesson, " +
                                    "tol1.beginTime, tol1.endTime, tow.typeOfWeek FROM `schedule` s " +
                                    "LEFT JOIN `group` g ON (g.idGroup = s.idGroup) " +
                                    "LEFT JOIN `faculty` f ON (f.idFaculty = g.idFaculty) " +
                                    "LEFT JOIN `speciality` sp ON (sp.idSpeciality = g.idSpeciality) " +
                                    "LEFT JOIN `audience` a ON (a.idAudience = s.idAudience) " +
                                    "LEFT JOIN `teacher` t ON (t.idTeacher = s.idTeacher) " +
                                    "LEFT JOIN `subject` sbj ON (sbj.idSubject = s.idSubject) " +
                                    "LEFT JOIN `day_of_week` dow ON (dow.idDayOfWeek = s.idDayOfWeek) " +
                                    "LEFT JOIN `type_of_lesson` tol ON (tol.idTypeOfLesson = s.idTypeOfLesson) " +
                                    "LEFT JOIN `time_of_lesson` tol1 ON (tol1.idTimeOfLesson = s.idTimeOfLesson) " +
                                    "LEFT JOIN `type_of_week` tow ON (tow.idTypeOfWeek = s.idTypeOfWeek) " +
                                    "WHERE 1 ORDER BY s.idSchedule");
                            while(resultSet.next()){
                            //задание переменных данными из запроса для вывода 
                                int idSchedule = resultSet.getInt(1);
                                String facultyName = resultSet.getString(2);
                                String facultyShortName = resultSet.getString(3);
                                int numberStudents = resultSet.getInt(4);
                                String groupName = resultSet.getString(5);
                                String specialityName = resultSet.getString(6);
                                String specialityShortName = resultSet.getString(7);
                                String audienceName = resultSet.getString(8);
                                String teacherSurname = resultSet.getString(9);
                                String teacherName = resultSet.getString(10);
                                String teacherPatronymic = resultSet.getString(11);
                                String subjectName = resultSet.getString(12);
                                String dayOfWeek = resultSet.getString(13);
                                String typeOfLesson = resultSet.getString(14);
                                String beginTime = resultSet.getString(15);
                                String endTime = resultSet.getString(16);
                                String typeOfWeek = resultSet.getString(17);
                                //заполнение строки 
                                sb.append(String.format("%d. %s (%s), %s - %s, %s, %s, %s %s %s, %s, %s \n", idSchedule, dayOfWeek, typeOfWeek,
                                        beginTime, endTime, groupName, audienceName, teacherSurname, teacherName,
                                        teacherPatronymic, subjectName, typeOfLesson) + "\n" + "----------------------------------\n");
                            }
                        //вывод строки с данными 
                        writer.write(sb.toString());
                        //отчистка райтера и строки
                        writer.flush();
                        sb.setLength(0);
                    }
                    catch(Exception ex){
                        System.out.println("Не получается подключиться к базе данных!");
                        System.out.println(ex);
                    }
                }
                if (request.equals("2")) {
                    try (Connection conn = DriverManager.getConnection(url, username, password)){
                    //создание запроса на вывод всех данных об учителях 
                        Statement statement = conn.createStatement();
                        ResultSet resultSet = statement.executeQuery("SELECT * FROM Teacher");
                        while(resultSet.next()){
                        //задание переменных данными из запроса для вывода 
                            int id = resultSet.getInt(1);
                            String name = resultSet.getString(2);
                            String lesson = resultSet.getString(3);
                             //заполнение строки 
                            sb.append(String.format("%d. %s - %s \n", id, name, lesson) + "\n" + "----------------------------------\n");
                        }
                        //вывод строки с данными 
                        writer.write(sb.toString());
                        //отчистка райтера и строки
                        writer.flush();
                        sb.setLength(0);
                    }
                    catch(Exception ex){
                        System.out.println("Не получается подключиться к базе данных!");
                        System.out.println(ex);
                    }
                }
                if (request.equals("3")) {
                    try (Connection conn = DriverManager.getConnection(url, username, password)){
                    //создание запроса на вывод всех данных о группах 
                        Statement statement = conn.createStatement();
                        ResultSet resultSet = statement.executeQuery("SELECT * FROM groups");
                        while(resultSet.next()){
                        //задание переменных данными из запроса для вывода 
                            int id = resultSet.getInt(1);
                            String groupName = resultSet.getString(2);
                             //заполнение строки 
                            sb.append(String.format("%d. %s\n", id, groupName) + "\n" + "----------------------------------\n");
                        }
                        //вывод строки с данными 
                        writer.write(sb.toString());
                         //отчистка райтера и строки
                        writer.flush();
                        sb.setLength(0);
                    }
                    catch(Exception ex){
                        System.out.println("Не получается подключиться к базе данных!");
                        System.out.println(ex);
                    }
                }

                // отправка ответа клиенту
                writer.write(sb.toString());
                writer.flush();
                sb.setLength(0);
            }
        }
        catch (IOException ex) {
            System.out.println("Не получается подключить пользователя.");
        }
    }
}
